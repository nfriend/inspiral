import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// Hides the system UI overlays.
void hideSystemUIOverlays() {
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Colors.transparent));
}
