import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SocialButton extends StatelessWidget {
  final String assetPath;
  final Uri linkHref;

  SocialButton({required this.assetPath, required this.linkHref});

  @override
  Widget build(BuildContext context) {
    return IconButton(
        onPressed: () async {
          await FirebaseAnalytics.instance.logEvent(
              name: 'social_button',
              parameters: {'linkHref': linkHref.toString()});

          await canLaunchUrl(linkHref)
              ? await launchUrl(linkHref)
              : throw 'Could not launch $linkHref';
        },
        icon: Image.asset(assetPath));
  }
}
