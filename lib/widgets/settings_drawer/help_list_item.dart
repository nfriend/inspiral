import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:inspiral/routes.dart';
import 'package:pedantic/pedantic.dart';

class HelpListItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text('Help'),
      onTap: () {
        unawaited(FirebaseAnalytics.instance.logEvent(name: 'help'));

        Navigator.pushNamed(context, InspiralRoutes.help);
      },
    );
  }
}
