import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:inspiral/models/entitlement.dart';
import 'package:inspiral/models/package.dart';
import 'package:inspiral/state/state.dart';
import 'package:inspiral/widgets/drawing_tools/action_button.dart';
import 'package:inspiral/widgets/drawing_tools/color_selector_thumbnail.dart';
import 'package:inspiral/widgets/drawing_tools/new_color_thumbnail.dart';
import 'package:inspiral/widgets/drawing_tools/selection_row.dart';
import 'package:pedantic/pedantic.dart';
import 'package:provider/provider.dart';
import 'package:tinycolor/tinycolor.dart';
import 'package:inspiral/util/custom_icons.dart';

class ToolsSelector extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final purchases = Provider.of<PurchasesState>(context, listen: false);
    final canvas = Provider.of<CanvasState>(context, listen: false);
    final isSelectingHole =
        context.select<CanvasState, bool>((canvas) => canvas.isSelectingHole);
    final isAutoDrawing = context.select<RotatingGearState, bool>(
        (rotatingGear) => rotatingGear.isAutoDrawing);
    final isDrawingCompletePattern = context.select<RotatingGearState, bool>(
        (rotatingGear) => rotatingGear.isDrawingCompletePattern);
    final rotatingGearIsVisible = context.select<RotatingGearState, bool>(
        (rotatingGear) => rotatingGear.isVisible);
    final isFixedGearLocked =
        context.select<FixedGearState, bool>((fixedGear) => fixedGear.isLocked);
    final areSnapPointsActive = context
        .select<SnapPointState, bool>((snapPoints) => snapPoints.areActive);
    final colors = Provider.of<ColorState>(context);
    final colorPicker = Provider.of<ColorPickerState>(context);
    final rotatingGear = Provider.of<RotatingGearState>(context, listen: false);
    final fixedGear = Provider.of<FixedGearState>(context, listen: false);
    final snapPoints = Provider.of<SnapPointState>(context, listen: false);

    return SelectionRows(rowDefs: [
      SelectionrRowDefinition(storageKey: 'tools', label: 'TOOLS', children: [
        ActionButton(
          icon: InspiralCustomIcons.select_hole,
          tooltipMessage: 'Select a pen hole',
          isActive: isSelectingHole,
          isDisabled: isAutoDrawing || !rotatingGearIsVisible,
          onButtonTap: () {
            canvas.isSelectingHole = !isSelectingHole;

            unawaited(FirebaseAnalytics.instance.logEvent(name: 'select_hole'));
          },
        ),
        ActionButton(
          icon: InspiralCustomIcons.forward_1,
          tooltipMessage: 'Rotate in place clockwise by one tooth',
          onButtonTap: () {
            rotatingGear.rotateInPlace(teethToRotate: -1);

            unawaited(
                FirebaseAnalytics.instance.logEvent(name: 'rotate_clockwise'));
          },
        ),
        ActionButton(
          icon: InspiralCustomIcons.backwards_1,
          tooltipMessage: 'Rotate in place counterclockwise by one tooth',
          onButtonTap: () {
            rotatingGear.rotateInPlace(teethToRotate: 1);

            unawaited(FirebaseAnalytics.instance
                .logEvent(name: 'rotate_counterclockwise'));
          },
        ),
        ActionButton(
          icon: Icons.refresh,
          tooltipMessage: 'Draw one rotation',
          isDisabled: isAutoDrawing,
          onButtonTap: () {
            canvas.isSelectingHole = false;
            rotatingGear.drawOneRotation();

            unawaited(
                FirebaseAnalytics.instance.logEvent(name: 'draw_one_rotation'));
          },
        ),
        isDrawingCompletePattern
            ? ActionButton(
                icon: Icons.pause,
                tooltipMessage: 'Pause auto-drawing',
                onButtonTap: () {
                  rotatingGear.stopCompletePatternDrawing();

                  unawaited(FirebaseAnalytics.instance
                      .logEvent(name: 'pause_auto_draw'));
                },
              )
            : ActionButton(
                icon: InspiralCustomIcons.rotate_complete,
                tooltipMessage: 'Draw complete pattern',
                onButtonTap: () {
                  canvas.isSelectingHole = false;
                  rotatingGear.drawCompletePattern();

                  unawaited(FirebaseAnalytics.instance
                      .logEvent(name: 'draw_complete_pattern'));
                },
              ),
        ActionButton(
          icon: isFixedGearLocked ? Icons.lock_open : Icons.lock,
          tooltipMessage:
              isFixedGearLocked ? 'Unlock fixed gear' : 'Lock fixed gear',
          isActive: isFixedGearLocked,
          onButtonTap: () {
            unawaited(FirebaseAnalytics.instance.logEvent(
                name: isFixedGearLocked
                    ? 'unlock_fixed_gear'
                    : 'lock_fixed_gear'));

            fixedGear.isLocked = !fixedGear.isLocked;
          },
        ),
        ActionButton(
          icon: Icons.adjust,
          tooltipMessage:
              areSnapPointsActive ? 'Hide snap points' : 'Show snap points',
          isActive: areSnapPointsActive,
          onButtonTap: () {
            snapPoints.areActive = !snapPoints.areActive;

            unawaited(FirebaseAnalytics.instance.logEvent(
                name: areSnapPointsActive
                    ? 'hide_snap_points'
                    : 'show_snap_points'));
          },
        ),
      ]),
      SelectionrRowDefinition(
          storageKey: 'canvasColor',
          label: 'CANVAS',
          children: [
            for (TinyColor color in colors.availableCanvasColors)
              FutureBuilder(
                  future: purchases
                      .isEntitledTo(Entitlement.custombackgroundcolors),
                  builder:
                      (BuildContext context, AsyncSnapshot<bool> snapshot) {
                    var showDeleteButton = snapshot.hasData &&
                        snapshot.data! &&
                        colors.showCanvasColorDeleteButtons &&
                        colors.availableCanvasColors.length > 1;

                    return ColorSelectorThumbnail(
                        color: color,
                        isActive: color.color == colors.backgroundColor.color,
                        onColorTap: () {
                          colors.showCanvasColorDeleteButtons = false;
                          colors.backgroundColor = color;

                          unawaited(FirebaseAnalytics.instance.logSelectContent(
                              contentType: 'canvasColor',
                              itemId: color.color.toString()));
                        },
                        onColorLongPress: () =>
                            colors.showCanvasColorDeleteButtons =
                                !colors.showCanvasColorDeleteButtons,
                        onColorDelete: () {
                          colors.removeCanvasColor(color);

                          unawaited(FirebaseAnalytics.instance.logEvent(
                              name: 'delete_canvas_color',
                              parameters: {'color': color.color.toString()}));
                        },
                        showDeleteButton: showDeleteButton);
                  }),
            NewColorThumbnail(
                title: 'New canvas color',
                entitlement: Entitlement.custombackgroundcolors,
                package: Package.custombackgroundcolors,
                showOpacity: false,
                initialColor: colorPicker.lastSelectedCustomCanvasColor.color,
                onPress: () {
                  colors.showCanvasColorDeleteButtons = false;

                  unawaited(FirebaseAnalytics.instance
                      .logEvent(name: 'new_canvas_color_dialog'));
                },
                onColorMove: (Color color) {
                  colorPicker.lastSelectedCustomCanvasColor = TinyColor(color);
                },
                onSelect: (color) {
                  colors.addAndSelectCanvasColor(TinyColor(color));

                  unawaited(FirebaseAnalytics.instance.logEvent(
                      name: 'new_canvas_color_selected',
                      parameters: {'color': color.toString()}));
                }),
          ]),
    ]);
  }
}
