import 'package:flutter/material.dart';
import 'package:inspiral/state/state.dart';
import 'package:provider/provider.dart';

/// A widget that provides a theme based on the currently selected
/// pen and background colors
class DynamicTheme extends StatelessWidget {
  DynamicTheme({required this.child});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    final colors = context.watch<ColorState>();

    var textTheme = colors.uiBackgroundColor.isDark()
        ? Typography.whiteHelsinki
        : Typography.blackHelsinki;

    var buttonStyle = ButtonStyle(
        shape: WidgetStateProperty.resolveWith((states) => StadiumBorder()),
        backgroundColor: WidgetStateProperty.resolveWith(
            (states) => colors.buttonColor.color),
        foregroundColor: WidgetStateProperty.resolveWith(
            (states) => colors.uiTextColor.color),
        overlayColor: WidgetStateProperty.resolveWith(
            (states) => colors.splashColor.color));

    var textButtonTheme = TextButtonThemeData(style: buttonStyle);

    var colorScheme = ColorScheme.fromSwatch().copyWith(
        secondary: colors.accentColor.color,
        brightness: colors.isDark ? Brightness.dark : Brightness.light);

    return Theme(
        data: ThemeData(
            useMaterial3: false,
            splashColor: colors.splashColor.color,
            primaryColor: colors.primaryColor.color,
            highlightColor: colors.highlightColor.color,
            textButtonTheme: textButtonTheme,
            primaryTextTheme: textTheme,
            colorScheme: colorScheme),
        child: child);
  }
}
